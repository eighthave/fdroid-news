---
layout: post
title: "TWIF 25: Tor Browser, Matrix-Raum-Verbesserungen und Spiegelserver-Beta-Test"
lang: de
edition: 25
author: "Coffee"
authorWebsite: "https://open.source.coffee"
fdroid: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #0d47a1; font-style: normal; font-weight: bold;">F-Droid</em>'
featuredv1: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.5ex; box-shadow: 0.1em 0.05em 0.1em rgba(0, 0, 0, 0.3); border-radius: 1em; color: black; background: linear-gradient(orange, yellow);">Featured</em>'
featured: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: orange; font-style: normal; font-weight: bold;">Featured</em>'
major: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #8ab000; font-style: normal; font-weight: bold;">Major</em>'
number_of_updated_apps: 114
mastodonAccount: "**[@fdroidorg@mastodon.technology](https://mastodon.technology/@fdroidorg)**"
twifTag: "**[#TWIF](https://mastodon.technology/tags/twif)**"
twifThread: "[TWIF submission thread](https://forum.f-droid.org/t/twif-submission-thread)"
matrixRoom: "[#fdroid:matrix.org](https://matrix.to/#/#fdroid:matrix.org)"
telegramRoom: "https://t.me/joinchat/AlRQekvjWDTuQrCgMYSNVA"
forum: "https://forum.f-droid.org"
---


Diese Woche In F-Droid {{ page.edition }}, Woche {{ page.date | date: "%V, %G" }} <a href="{{ site.baseurl }}/feed.xml"><img src="{{ site.baseurl }}/assets/Feed-icon-16x16.png" alt="Feed"></a>

In dieser Ausgabe: Tor Browser im Guardian Project, Matrix-Raum verbessert, F-Droid-Werkzeugleistenverbesserung, Beta-Spiegelserver bereit zum Test sowie 11 neue Apps und alle Rekorde brechende **{{ page.number_of_updated_apps }}** Aktualisierungen.
<!--more-->

[F-Droid](https://f-droid.org/) ist ein [Repository](https://f-droid.org/de/packages/) verifizierter [Free & Open Source](https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software) Android Apps, ein [Client](https://f-droid.org/app/org.fdroid.fdroid), um darauf zuzugreifen, sowie ein ganzes "App Store Set", das alle notwendigen Werkzeuge zum Einrichten und Betreiben eines App Stores bietet. Es ist ein gemeinschaftlich geführtes freies Softwareprojekt, das von einer Vielzahl von Mitwirkenden weiterentwickelt wird. Das ist ihre Geschichte der letzten Woche.

#### Tor Browser im Guardian Project Repository abrufbar

Eine Alpha-Version des neuen Tor Browser steht jetzt im Guardian Project
Repository zur Verfügung. Es ist dieselbe APK wie die von der Tor
Project-Internetseite. Das Guardian Project arbeitet daran, sie
[reproduzierbar](https://reproducible-builds.org) erstellt zu bekommen, und
sobald es soweit ist, werden wir uns bemühen, den reproduzierbaren Tor
Browser-Build auf F-Droid hinzuzufügen.

In der Zwischenzeit suchen wir nach einem Freiwilligen, der eine nicht
reproduzierbar erstellte Version zu F-Droid hinzufügen möchte. Es wäre
großartig, wenn jemand mit Erfahrung in Fennec-Builds versuchen könnte, Tor
Browser zu bauen.

#### Wachsende Präsenz auf Matrix

Wir hatten einen IRC-Raum auf Freenode seit mindestens 2014 und dieser Raum
wurde zu Matrix übergeleitet, als Matrix mit der Überbrückung von Freenode
begann. Während des letzten Jahres haben wir die Matrixhälfte des Raums
angepasst, indem wir einen hübschen Raum-Avatar neben anderen Dingen
hinzugefügt haben. aber das Ausmaß der Anpassungen auf diese Weise ist
limitiert.

So machten wir uns diese Woche daran, einen Matrix-Raum zu schaffen und
manuell eine Brücke zwischen ihm und IRC zu schlagen. Das Ergebnis davon
ist, dass wir nun einen Matrix-Raum besitzen, der vollständig von uns
kontrolliert wird. Das ermöglichte uns, den Verlauf öffentlich sichtbar zu
machen, so kann nun jeder die Protokolle auf
[https://view.matrix.org/alias/#fdroid:matrix.org](https://view.matrix.org/alias/%23fdroid:matrix.org)
zurückverfolgen. Wir haben auch den Raum im Raumverzeichnis von Matrix.org
gelistet, damit Leute, die nach F-Droid suchen, uns auch tatsächlich finden.

Schließlich wird der Raum jetzt auf
[#fdroid:matrix.org](https://matrix.to/#/#fdroid:matrix.org)
erreicht. Dieser Alias gehörte früher einem fremden F-Droid-Raum, der auf
Matrix seit Januar 2017 geöffnet war. Die Betreiber haben uns
dankenswerterweise gestattet, den Alias für unseren Raum zu nutzen, wofür
wir ihnen herzlich danken.

#### Kompakte Werkzeugleiste im F-Droid-Client

**[@Licaon_Kter](https://forum.f-droid.org/u/Licaon_Kter)** arbeitet an einer modifizierten Werkzeugleiste im F-Droid-Client, die mehr Platz für Text bietet, indem die inaktiven Elemente verkleinert werden. Dies sollte die Probleme lösen, dass Texte in anderen Sprachen als Englisch beschnitten werden. Sie können sie [hier](https://gitlab.com/fdroid/fdroidclient/merge_requests/756) in Aktion sehen.

#### Tutanota auf F-Droid im Blickpunkt des Linux Journal

Tutanota und die Geschichte, wie sie ihre App passend für F-Droid
gestalteten, wurde im Linux Journal
[beleuchtet](https://www.linuxjournal.com/content/foss-project-spotlight-tutanota-first-encrypted-email-service-app-f-droid).
Natürlich wird Ihnen das, wenn Sie [unseren Blog verfolgen]({{ site.baseurl
}}{% post_url 2018-09-03-replacing-gcm-in-tutanota %}), sehr bekannt
vorkommen.

#### Beta-Spiegelserver bereit zum Test

Tetaneutral ist ein nicht kommerzieller ISP mit Sitz in Toulouse,
Frankreich. Sie haben uns das Hosting eines Spiegelservers sowohl unseres
F-Droid-Hauptrepository als auch des Archivs angeboten. Wir testen momentan
dessen Einrichtung und, falls Sie ihn selbst ausprobieren möchten, [tippen
Sie
hier](https://fdroid.tetaneutral.net/fdroid/repo?fingerprint=43238D512C1E5EB2D6569F4A3AFBF5523418B82E0A3ED1552770ABB9A9C9CCAB)
auf Ihrem mobilen Gerät, um ihn als "Benutzer-Spiegel" für f-droid.org
hinzuzufügen. (Die Verknüpfung wird nicht auf einem PC, oder wenn keine
F-Droid-App installiert ist, funktionieren.)

#### Neue Apps

* **[Cool Mic](https://f-droid.org/app/cc.echonet.coolmicapp)** ist eine
  Open Source Icecast-Quelle. Sie liefert Live-Streams von Audiodaten, die
  über Ihr Android-Gerätemikrofon oder angeschlossene Mikrofone erfasst
  werden, an jeden Icecast-Server.

* Wollten Sie jemals ein fiktives Setting — für Ihre nächste Erzählung,
  einen Comic, Film oder was auch immer — erschaffen, fanden es aber
  schwierig, alles organisiert zu bekommen? **[World
  Scribe](https://f-droid.org/app/com.averi.worldscribe)** vereinfacht den
  Schaffungsprozess, indem es den Überblick über jedes wichtige Element in
  Ihrer Welt behält, inklusive der Art und Weise, auf die sie miteinander
  verknüpft sind.

* **[Cryptolitycs](https://f-droid.org/app/com.fproject.cryptolitycs)**
  liefert einen schnellen und einfachen Zugriff auf krypto-währungsbezogene
  Informationen und ermöglicht dem Nutzer, den Krypto-Währungsmarkt zu
  verfolgen.

* **[Identify Dog Breeds](https://f-droid.org/app/com.jstappdev.dbclf)**:
  Identifizieren Sie über einhundert verschiedene Hunderassen mit Ihrem
  Smartphone.

* **[CPU Info](https://f-droid.org/app/com.kgurgul.cpuinfo)**: Informationen
  zur Hard- und Software des Geräts.

* **[XOverrideHeadphoneJackDetection](https://f-droid.org/app/de.antonarnold.android.xoverrideheadphonejackdetection)**
  ist ein XPosed-Modul, dass es möglich macht, manuell die
  Kopfhöreranschlusserkennung eines Android-Geräts auszuschalten.

* **[Calcvac](https://f-droid.org/app/de.drhoffmannsoftware.calcvac)**:
  Longitudinale, eindimensionale Druckprofile in Vakuumröhren berechnen.

* **[SQRL - Main](https://f-droid.org/app/org.ea.sqrl)**: Eine
  Implementierung für SQRL (Secure Quick Reliable Login).

* **[Flyve MDM Agent](https://f-droid.org/app/org.flyve.mdm.agent)**: Mobile
  Geräte und Anwendungen effektiv verwalten und sichern.

* **[Quickly quit](https://f-droid.org/app/quickly.quit)**: Schnell beenden,
  womit Sie sich beschäftigten.

* {{ page.featured }} **[Manyverse](https://f-droid.org/app/se.manyver)**:
  Endlich ist ein mobiler Client für das _[Secure
  Scuttlebutt](https://ssbc.github.io/secure-scuttlebutt/)_-Protokoll auf
  F-Droid gelandet! SSB ist ein Peer-to-Peer-Protokoll und soziales
  Netzwerk, das für die Nutzung offline und ohne Versorgungsnetz optimiert
  ist. Sie können Nachrichten lokal über Bluetooth oder ein lokales WLAN
  teilen, über das Internet direkt mit Ihren Freunden austauschen oder
  wahlweise zentralisiertere Knoten, sogenannte "Pubs", anzapfen.

    Für tiefergehende Informationen über den Entwickler, André Staltz, und seine Motivation können Sie den TEDxGeneva-Talk [Es wird Zeit, unser eigenes Internet zu bauen](https://www.youtube.com/watch?v=UjfWAbGfPh0) und das jüngste [Das soziale Web neu erfinden](https://www.youtube.com/watch?v=8GE5C9-RUpg) anschauen. Wenn Sie mehr dazu neigen zu lesen, können Sie sich seine ansteckenden Blog-Beiträge unter dem Titel [Das Sterben des Web begann 2014, wie es dazu kam](https://staltz.com/the-web-began-dying-in-2014-heres-how.html) und den Nachfolger [Ein Plan das Web vor dem Internet zu retten](https://staltz.com/a-plan-to-rescue-the-web-from-the-internet.html) ansehen.

#### Aktualisierte Apps

Insgesamt wurden diese Woche **{{ page.number_of_updated_apps }}** Apps
aktualisiert. Hier die Höhepunkte:

* {{ page.major }} Viele Änderungen in **[VPN
  Hotspot](https://f-droid.org/app/be.mygod.vpnhotspot)**, deren Updates wir
  seit Anfang Juni nicht mehr übernommen haben. Geradewegs von 1.3.2 auf
  2.0.3 aktualisiert, besitzt sie verfeinerte Masquerading-Regeln, den Start
  beim Boot, ein dunkles Design und die Android-TV-Unterstützung,
  Identifizierung und Blockierung unerwünschter Clients, Aufzeichnung der
  Bandbreite pro Client, und eine Masse an Verbesserungen der
  Benutzeroberfläche und Fehlerlösungen. Beachten Sie, dass die
  Funktionalität dieser App ohne Root-Zugriff eingeschränkt ist.

* **[PasteBin](https://f-droid.org/app/com.anoopknr.pastebin)** ist ein
  Client für Ubuntu Pastebin. Er wurde von 1.0 auf 1.5 aktualisiert, nun mit
  einer stabilen und reaktionsfähigen Benutzeroberfläche, neuen "letzter
  Post"- und "Über"-Optionen, neu angepassten Werkzeug- und Menüleisten und
  den üblichen Fehlerkorrekturen.

* **[DuckDuckGo Privacy
  Browser](https://f-droid.org/app/com.duckduckgo.mobile.android)**, von
  5.10.3 auf 5.10.4 aktualisiert, ergänzt Optimierungen in seiner Umsetzung
  der HTTPS-Höhereinstufung.

*   **[TrebleShot](https://f-droid.org/app/com.genonbeta.TrebleShot)**
    ermöglicht den Versand und Empfang von Dateien über die zur Verfügung
    stehenden Verbindungen und das Pausieren und Fortsetzen des
    Übertragungsvorgangs selbst bei Auftreten eines Fehlers. Dieses Update
    von 1.2.7.2 auf 1.2.7.5 bringt die verbesserte Unterstützung von Android
    9 (Pie) mit, ein aktualisertes & ein dunkles Design sowie ein
    verbessertes Aussehen der App und des QR-Scanners, wie auch neue und
    aktualisierte Übersetzungen und Fehlerlösungen.

* **[PocketMaps](https://f-droid.org/app/com.junjunguo.pocketmaps)** ist ein
  Kartenbetrachter mit Navigationssystem und Unterstützung für den
  Offline-Gebrauch. Diese Aktualisierung von 2.0 auf 2.3 ermöglicht das
  Laden von GPX-Dateien, um eine Analyse und Route anzuzeigen, die
  Stummschaltung der Sprachführung bei Telefonanrufen, das Sichtbarmachen
  von Downloads in der Benachrichtigungsleiste, akzeptiert jetzt geo: Links
  aus anderen Apps, und enthält diverse UI-Verbesserungen.

*   **[Calculator](https://f-droid.org/app/com.simplemobiletools.calculator)**
    4.2.0 hat einige aktualisierte Bibliotheken + Übersetzungen.

* **[File
  Manager](https://f-droid.org/app/com.simplemobiletools.filemanager)**
  4.3.1 hat einige Korrekturen und Verbesserungen der Übersetzungen.

* **[Gallery](https://f-droid.org/app/com.simplemobiletools.gallery)** 4.6.5
  fügt die Notch-Unerstützung für Android 9 (Pie) hinzu, ein schnelleres
  Video-Spulen, mittels Fingerzug an der unteren SeekBar, eine andere Art
  der Anzeige von GIFs im Vollbildmodus, einen neuen Umschalter, um die
  Anzeige der bestmöglichen Bildqualität auszuprobieren, lässt
  Favoriteneinträge nach Verschieben markiert und hat viele kleinere
  Verbesserungen und Korrekturen.

* **[Elementary](https://f-droid.org/app/com.ultramegatech.ey)** ist eine
  einfache Periodentafel und ein Elementeverzeichnis. Version 0.9.0 braucht
  jetzt mindestens Android 4.0 (zuvor 2.3) und besitzt aktualisierte Videos
  für Argon, Zinn und Dubnium.

* {{ page.major }}
  **[Qz](https://f-droid.org/app/com.vonglasow.michael.qz)** (ausgesprochen
  "kju:s") verbindet sich mit einem RDS/TMC-Empfänger, um
  Live-Verkehrsnachrichten zu erhalten, die Ihnen helfen, Staus zu
  vermeiden. Version 2.0.0 kam heraus mit der Möglichkeit, neue Meldungen
  als TraFF-Feeds an Anwendugen zu senden, die das unterstützen,
  Aktualisierungen der Standorttabellen und Behebung eines Fehlers, der dazu
  führte, dass die Nachrichtenlistenoberfläche manche Aktualisierungen
  verpasste.

* {{ page.major }}
  **[Wownerujo](https://f-droid.org/app/com.wownero.wownerujo)** ist ein
  Fork der Kryptowährung Monero mit grundlegenden Veränderungen. Nach
  Aktualisierung von 1.6.4.0 auf 2.3.0.1 basiert die App nun auf Wownero
  v0.3.0.0 Cool Cage.

* **[Tricky
  Tripper](https://f-droid.org/app/de.koelle.christian.trickytripper)** ist
  ein offline-kompatibles Hilfsmittel zur Verwaltung von Reisekosten. 1.6.0
  repariert den Online-Abruf von Wechselkursen, allerdings ohne
  Unterstützung für den salvdorianischen Colon und sambischen Kwacha.

* **[Tutanota](https://f-droid.org/app/de.tutao.tutanota)** 3.37.2 enthält
  aktualisierte allgemeine Geschäftdbedingungen.

* **[EtchDroid](https://f-droid.org/app/eu.depau.etchdroid)** hilft dabei,
  Images auf USB-Laufwerke zu schreiben. Sie können sie verwenden, um
  bootfähige GNU/Linux-USB-Laufwerke zu erstellen, wenn Ihr Laptop tot ist
  und Sie im Nirgendwo sind. Root ist nicht notwendig. Version 1.3 bringt
  ein dunkles Design, die Möglichkeit, Images aus einem Dateimanager zu
  öffnen, und ergänzt eine Notlösung für ein Problem in Android 9 (Pie).

* **[NetGuard](https://f-droid.org/app/eu.faircode.netguard)** wurde von
  2.207 auf 2.212 aktualisiert, nun mit Prüfung auf ständig mitlaufendes
  VPN, aktualisierten Übersetzungen und einigen Korrekturen.

* **[Tachiyomi](https://f-droid.org/app/eu.kanade.tachiyomi)** ist ein
  Manga-Reader. Version 0.8.0 bringt eine ganze Reihe Änderungen, fügt einen
  neuen Reader mit vielen Korrekturen hinzu, bietet GIF-Unterstützung, zeigt
  alle Einträge in der Bibliothek, selbst wenn ihre Erweiterungen nicht
  installiert sind, und enthält verschiedene Fehlerlösungen.

* **[Conversations](https://f-droid.org/app/eu.siacs.conversations)** ist
  ein Instant Messenger mit Unterstützung von Gruppen-Chats, die auf dem
  XMPP-Protokoll beruhen. Ab Version 2.3.0+fcr bringt er die
  TLSv1.3-Unterstützung mit und kann jetzt im Audio-Player den Bildschirm
  abschalten & zum Kopfhörer umschalten, den Vordergrunddienst standardmäßig
  in Android 8 (Oreo) aktivieren, in den Kontakt- und
  Konferenz-Detailansichten die Mediendateien nach Kontakt anzeigen, vor dem
  Versand von Mediendateien eine Vorschau anzeigen und eine Bestätigung
  abfragen, verwendet nun eine einheitliche Farbwahl, repariert "Nicht
  stören" auf Android 8+ und ergänzt eine Wahl der Videoqualität.

* {{ page.featured }}}}} **[Mastalab](https://f-droid.org/app/fr.gouv.etalab.mastodon)** ist ein leistungsfähiger Multi-Account Client für [Mastodon](https://joinmastodon.org). Viele neue Funktionen sind in diesem Sprung von 1.13.6 auf 1.15.2, jetzt mit Video- und Peertube-Unterstützung:
  * Langer Druck auf den Bookmark-Button, um Toots mit einem anderen Konto zu bookmarken.
  * Verifizierte URL in Profilen (derzeit nur auf mastodon.social verfügbar)
  * Unterstützung für das Blockieren ganzer Instanzen
  * Erlauben der Freigabe von Videos
  * Anleitungsvideos im Menü
  * Verwalten von Peertube-Videos (Vollbild + Kommentare)
  * Der Bildschirmname wird im nicht kompakten Modus nicht angezeigt
  * Filterproblem mit lokaler Zeitleiste
  * Verfolgen von Peertube-Instanzen mit der Möglichkeit, Videos mit Mastodon-Konten zu kommentieren
  * Direkte Nachrichten in einer Spalte (kann in den Einstellungen ausgeblendet werden)
  * Viele weitere Änderungen, Verbesserungen und Bugfixes

* **[Markor](https://f-droid.org/app/net.gsantner.markor)** 1.3.0 ist mit
  einem "Auto"-Thema erhältlich, das automatisch zwischen dunklem und hellem
  Thema abhängig von der Tageszeit umschaltet. Es unterstützt auch Chrome
  Custom Tabs, beginnt nun mit der Bearbeitung von Dokumenten an der zuletzt
  bekannten Cursorposition und ermöglicht die Hervorhebung von Links im
  Klartextformat. Das lange Drücken auf ein Bild fügt nun img-src mit
  maximaler Höhe hinzu, das lange Drücken der "Spezialtaste" springt nach an
  oberere/untere Ende und das lange Drücken von "Extern öffnen" öffnet die
  Kontextsuche. Darüber hinaus finden Sie Verbesserungen bei TextActions und
  Representation sowie Bugfixes.

* **[OsmAnd~](https://f-droid.org/app/net.osmand.plus)** 3.2.2.2 behebt
  einen Absturz beim Start, der bei nicht-lateinischen Karten auftrat,
  verbessert die Rendering-Geschwindigkeit auf Android 8.0-Geräten,
  unterstützt die Bearbeitung von Polygon-(Nicht-Änderungs-)Objekten und
  fügt den Aktionen im Kontextmenü des Entfernungsmess-Plugins eine
  Schaltfläche "Messen" hinzu.

* **[Open Camera](https://f-droid.org/app/net.sourceforge.opencamera)** 1.44.1 ergänzt eine Menge neuer Leistungen:
  * Neuer Fotomodus "NR" Rauschreduktion, nimmt eine Fotoreihe auf, wertet sie aus und führt sie zur Rauschreduktion zusammen (nur mit Camera2 API, nur auf High-End-Geräten unterstützt)
  * Neuer Fotomodus "Focus {}" zur Aktivierung von Focus Bracketing (nur mit Camera2 API)
  * Neue Option in Einstellungen/"Ghost image", um entweder das zuletzt aufgenommene Foto oder ein auf dem Gerät ausgewähltes Bild zu überlagern
  * Neue Option unter Einstellungen/Bildschirm-GUI/"Lautstärkepegel anzeigen", um während der Videoaufzeichnung den Lautstärkepegel auf dem Bildschirm anzuzeigen
  * Neue Option unter Einstellungen/Videoeinstellungen/"Video flat (log) profile", um Videos mit kontrastarmen Profil aufzunehmen (nur mit Camera2 API)
  * Neue Optionen unter Einstellungen/"Verarbeitungseinstellungen..." zur Festlegung der Filteroptionen Edge-Modus und Rauschreduktionsmodus (nur mit Camera2 API)
  * Neue Option Einstellungen/Fotoeinstellungen/"Längenmaß", um die Verwendung von Fuß anstatt Meter für die GPS-Höhe in Fotostempeln und Videountertiteln zu ermöglichen
  * Blitzsymbol auf dem Bildschirm (zur Anzeige, ob der Blitz auslösen wird oder nicht) jetzt vom Bildschirmblitz unterstützt
  * Neue Camera2 API Debug-Option unter Einstellungen/"Fotoeinstellungen", um die Fotoaufnahme während der Videoaufnahme-Funktion zu deaktivieren (wenn Ihr Gerät Probleme macht, Videos bei aktivierter Camera2 API aufzuzeichnen, versuchen Sie das zu deaktivieren)
  * Camera2-Unterstützung für hochaufgelöste Fotoauflösungen (erforderlich, damit auf einigen Geräten die höchste Auflösung unterstützt wird, z. B. Nokia 6, Samsung Galaxy S6)
  * Starten nach Vorhaben erkennt nun Aufträge für die Front- oder Rückkamera
  * Video-Bitratenoptionen für 150Mbps und 200Mbps (funktioniert u. U. nicht auf allen Geräten)
  * Neue Video-Tonquellen-Optionen: UNVERARBEITET (benötigt Android 7) und `STIMMENERKENNUNG`
  * Unvollständige Griechisch-Übersetzung (Dankeschön an Wasilis Mandratzis-Walz)
  * Neue Option, um die Anzeige des "Was gibt's Neues"-Dialog dauerhaft abzuschalten (unter Einstellungen/Bildschirm-GUI/ "Was gibt's Neues-Dialog anzeigen")
  * _Viele_ weitere Aktualisierungen und Fehlerlösungen

* {{ page.major }} Anscheinend waren wir seit 2 Jahren nicht in der Lage
  **[PDF Converter](https://f-droid.org/app/swati4star.createpdf)** zu
  aktualisieren. Jetzt haben wir 8.3.3 im Repository (zuvor 1.0) mit zu
  vielen Änderungen, um sie aufzuzählen. Von Bedeutung ist, dass wir auch
  einige ältere Versionen aufgelesen haben und dass Version 2.5 die letzte
  ist, die auf Android 4.4 installiert werden kann. Alle Versionen danach
  benötigen mindestens Android 5.0.

#### Anregungen und Rückmeldung

Haben Sie wichtige Updates einer App, über die wir schreiben sollten? Senden
Sie Ihre Tipps über [Mastodon](https://joinmastodon.org) ein! Senden Sie sie
an {{ page.mastodonAccount }} und denken Sie an einen {{ page.twifTag
}}-Tag. Oder nutzen Sie den {{ page.twifThread }} im Forum. Einsendeschluss
für das nächste TWIF ist **Donnerstag** 12:00 UTC.

Ein allgemeines Feedback kann ebenfalls über Mastodon abgegeben werden oder,
wenn Sie gerne an einem Live-Chat teilnehmen möchten, dann finden Sie uns in
**#fdroid** auf [Freenode](https://freenode.net), auf Matrix über {{
page.matrixRoom }} oder auf [Telegram]({{ page.telegramRoom }}). Alle diese
Orte werden miteinander verbunden, Sie haben also die Wahl. Sie können sich
uns auch im **[Forum]({{ page.forum }})** anschließen.
